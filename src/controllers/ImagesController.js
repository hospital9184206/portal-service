const fs = require('fs')
const path = require('path')
const folderName = process.env.FOLDER_NAME
const dirPath = path.join(__dirname, '..', '..', '..', folderName);

const dayjs = require('dayjs')

exports.getPath = () => async (req, res, next) => {
  req.dirPath = dirPath
  next()
}


exports.getAllImages = () => async (req, res, next) => {
  let files = []

  const fileList = fs.readdirSync(req.dirPath)

  for (const file of fileList) {
    const imgData = fs.readFileSync(`${req.dirPath}/${file}`, {encoding: 'base64'});
    const name = `${req.dirPath}/${file}`

    files.push({
      name: file,
      base64: imgData,
      path: name
    }) 
  }
  req.file = files
  next()
}

exports.filterEKHImages = (isFind) => async (req, res, next) => {
  const { textValue } = req.body
  let today = dayjs().format('YYYYMMDD')
  let userNames

  if(isFind && textValue){
    userNames = await req.file.filter((values) => values.name.includes('ekh_') && values.name.includes(textValue))
  } else {
    userNames = await req.file.filter((values) => values.name.includes('ekh_') && values.name.includes(today))
  }
  req.filterFile = userNames
  next()
}

exports.getSomethingImages = () => async (req, res, next) => {
  const id = `${req.params.id}.PNG`
  let files = []

  const fileList = fs.readdirSync(req.dirPath)

  for (const file of fileList) {
    const filePath = path.join(req.dirPath, file);
    const fileStat = fs.statSync(filePath);

    if (fileStat.isDirectory()) {
      next()
    } else if (file.endsWith(id)) {
      files.push({
        name: file,
        path: filePath
      })
    }
  }
  req.file = files
  next()
}

exports.DeleteSomethingImages = () => async (req, res, next) => {
  const { textValue } = req.body

  let isDel = ''
  let messageDel = ''

  const dirPath = path.join(__dirname, '..', '..', '..', folderName, textValue);
  try {
    await fs.unlinkSync(dirPath);
      isDel = true
      messageDel = 'ลบรูปสำเร็จ'
  } catch (error) {
    isDel = false
    messageDel = 'ไม่สามารถลบรูปได้โปรดติดต่อ Admin'
  }
  req.isDelSuccess = isDel
  req.messageDel = messageDel
  next()
}

exports.JobGetAllImages = async () => {
  let files = []

  const fileList = await fs.readdirSync(dirPath)
  
  for (const file of fileList) {
    const imgData = fs.statSync(`${dirPath}/${file}`);
    const name = `${dirPath}/${file}`

    files.push({
      name: file,
      lastNmae: dayjs(imgData.atime).format('YYYYMMDDHHmmss'),
      path: name
    }) 
  }

  return files
}

exports.JobFilterImages = async (files) => {
  return await files.filter((values) => !values.name.includes('ekh'))
}

exports.JobRenameImages = async (files) => {
  var array=[".png", ".PNG", ".jpg", ".JPG", ".jpeg", ".JPEG"];
  for (const file of files) {
    let nameFile = file.name.replace(new RegExp(array.join("|")),"")
    let extension = file.name.replace(/^.*\./, "")
    await fs.renameSync(file.path, `${dirPath}/ekh_${nameFile}_${file.lastNmae}.${extension}`); 
  }
}
