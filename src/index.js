require('./utils/dotenvUtil')
const cron = require('node-cron');

const everyMinuteJob =  require('./jobs/everyMinuteJob')

const express = require("express");
const app = express();

const port = process.env.PORT || 3000;

const router = require('./routers')

const {
  // sharedDir,
  // port,
  // hostname,
  origins
} = require('./configs/appConfig')

app.route('').get( (req, res) => {
  res.send('Hello from Node.js RESTful API');
});

app.use(express.json())
app.use(express.urlencoded());

app.use((req, res, next) => {
  let origin = `${req.protocol}://${req.headers.host}`
  if (!origins.includes(origin)) origin = origins[0]

  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Origin', origin)
  res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate, max-age=0')
  res.header('X-Frame-Options', 'deny')
  res.header('X-Content-Type-Options', 'nosniff')
  next()
})

app.use(router)

app.listen(port, () => {
  console.log('server is running on port: ', port);
});


const startOfDailyJob = cron.schedule(
	process.env.SET_TIME_JOB,
	function () {
    everyMinuteJob.everyMinuteJob()
	},
	null,
	true,
	'Asia/Bangkok'
);

startOfDailyJob.start()
