const appConfig = {
  port: Number(process.env.APP_PORT),
  hostname: process.env.APP_HOSTNAME,
  sharedDir: process.env.APP_SHARED_DIR,
  origins: JSON.parse(process.env.APP_ORIGINS),
  cookie: {
    secure: process.env.APP_COOKIE_SECURE === 'true',
    samesite: process.env.APP_COOKIE_SAMESITE
  }
}

module.exports = appConfig
