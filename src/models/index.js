// const { sequelize, DataTypes } = require('sequelize')
const DataTypes = require('sequelize')

// const sequelizeUtil = require('../utils/sequelizeUtil')

module.exports = (type = 'MASTER') => { 
    let sequelizeUtil
    switch (type) {
        case 'MASTER':
            sequelizeUtil = require('../utils/sequelizeUtil')
            break
        case 'TEST':
            console.log('=============== TEST')
            break
    }

    const USER = require('./USER')(sequelizeUtil, DataTypes)

    return  { 
        USER 
    }
}