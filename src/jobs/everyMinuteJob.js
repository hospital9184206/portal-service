const imagesController = require('../controllers/ImagesController')

exports.everyMinuteJob = async () => {
    const fileAll = await imagesController.JobGetAllImages()
    const filterFile = await imagesController.JobFilterImages(fileAll)
    await imagesController.JobRenameImages(filterFile)
}