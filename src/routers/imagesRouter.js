const express = require('express')
const Router = express.Router()

const imagesController = require('../controllers/ImagesController')

Router.get('/',
  imagesController.getPath(),
  imagesController.getAllImages(),
  imagesController.filterEKHImages(false),
  (req, res, next) => {
    req.payload = {
      data: req.filterFile,
      isData: req.filterFile.length === 0 ? false : true,
      message: 'Get All User'
    }
    res.json(req.payload)
    next()
  }
)

Router.post('/',
  imagesController.getPath(),
  imagesController.getAllImages(),
  imagesController.filterEKHImages(true),
  (req, res, next) => {
    req.payload = {
      data: req.filterFile,
      isData: req.filterFile.length === 0 ? false : true,
      message: 'Get Something User'
    }
    res.json(req.payload)
    next()
  }
)

Router.post('/delete',
  imagesController.getPath(),
  imagesController.DeleteSomethingImages(),
  (req, res, next) => {
        req.payload = {
            isDel: req.isDelSuccess,
            messageDel: req.messageDel,
            message: 'Delete Something User'
        }
        res.json(req.payload)
        next()
    }
)


module.exports = Router;