const express = require('express')
const Router = express.Router()

Router.use('/images', require('./imagesRouter'))

module.exports = Router;